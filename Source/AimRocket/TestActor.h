// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/Material.h"
#include "Materials/MaterialInterface.h"
#include "Components/SceneComponent.h"
#include "TestActor.generated.h"

class UStaticMeshComponent;
class UMaterial;
class UMaterialInterface;
class USceneComponent;

UCLASS()
class AIMROCKET_API ATestActor : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* Mesh = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* MeshHomingTarget = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = Mesh)
	UMaterialInterface* MaterialDefault = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = Mesh)
	UMaterialInterface* MaterialWhenTargeted = nullptr;
	
public:	
	// Sets default values for this actor's properties
	ATestActor();

	void ChangeColorOnLocked();

	void ChangeColorOnUnlocked();

	UStaticMeshComponent* GetMeshHomingTarget() { return MeshHomingTarget; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
