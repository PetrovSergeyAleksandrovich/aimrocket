// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AimRocketProjectile.h"
#include "ProjectileComponent.generated.h"

class AAimRocketProjectile;

UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UProjectileComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UProjectileComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TArray<TSubclassOf<AAimRocketProjectile>> Projectiles;

	int32 GetProjectilesAmount() { return Projectiles.Num(); };
};
