// Copyright Epic Games, Inc. All Rights Reserved.

#include "AimRocketCharacter.h"
#include "AimRocketProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"

DEFINE_LOG_CATEGORY_STATIC(LogAimRocketCharacter, All, All)

//////////////////////////////////////////////////////////////////////////
// AAimRocketCharacter

AAimRocketCharacter::AAimRocketCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	TurnRateGamepad = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

}

void AAimRocketCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

}

//////////////////////////////////////////////////////////////////////////// Input

void AAimRocketCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("PrimaryAction", IE_Pressed, this, &AAimRocketCharacter::OnPrimaryAction);

	// Bind aim event
	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &AAimRocketCharacter::OnAimAction);
	
	// Change Projectile
	PlayerInputComponent->BindAction("IndexUp", IE_Pressed, this, &AAimRocketCharacter::ChangeIndexUp);
	PlayerInputComponent->BindAction("IndexDown", IE_Pressed,this, &AAimRocketCharacter::ChangeIndexDown);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("Move Forward / Backward", this, &AAimRocketCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Move Right / Left", this, &AAimRocketCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "Mouse" versions handle devices that provide an absolute delta, such as a mouse.
	// "Gamepad" versions are for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn Right / Left Mouse", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Look Up / Down Mouse", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn Right / Left Gamepad", this, &AAimRocketCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("Look Up / Down Gamepad", this, &AAimRocketCharacter::LookUpAtRate);
}

void AAimRocketCharacter::OnPrimaryAction()
{
	// Trigger the OnItemUsed Event
	OnUseItem.Broadcast();
}

void AAimRocketCharacter::OnAimAction()
{
	UE_LOG(LogAimRocketCharacter, Display, TEXT("Aim pressed"));
	OnAimItem.Broadcast();
}

void AAimRocketCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnPrimaryAction();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AAimRocketCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

void AAimRocketCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AAimRocketCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AAimRocketCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void AAimRocketCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

bool AAimRocketCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AAimRocketCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AAimRocketCharacter::EndTouch);

		return true;
	}
	
	return false;
}

void AAimRocketCharacter::ChangeIndexUp()
{
	if (CurrentWeapon)
	{
		const auto ProjectileComp = Cast<UProjectileComponent>(CurrentWeapon->ProjectileComponent.GetDefaultObject());
		if (ProjectileComp)
		{
			if (++CurrentWeapon->ProjectileIndex >= ProjectileComp->GetProjectilesAmount())
			{
				CurrentWeapon->ProjectileIndex = ProjectileComp->GetProjectilesAmount() - 1;
			}
			UE_LOG(LogAimRocketCharacter, Display, TEXT("%i"), CurrentWeapon->ProjectileIndex);
		}
	}
}

void AAimRocketCharacter::ChangeIndexDown()
{
	if (CurrentWeapon)
	{
		if (--CurrentWeapon->ProjectileIndex <= 0)
		{
			CurrentWeapon->ProjectileIndex = 0;
		}
		UE_LOG(LogAimRocketCharacter, Display, TEXT("%i"), CurrentWeapon->ProjectileIndex);
	}
}
