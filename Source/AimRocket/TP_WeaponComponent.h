// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Actor.h"
#include "AimRocketProjectile.h"
#include "Animation/AnimMontage.h"
#include "Sound/SoundBase.h"
#include "TestActor.h"
#include "ProjectileComponent.h"
#include "TP_WeaponComponent.generated.h"

class AAimRocketCharacter;
class AAimRocketProjectile;
class UAnimMontage;
class USoundBase;
class ATestActor;
class UProjectileComponent;
class UStaticMeshComponent;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UTP_WeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/** Projectiles to spawn */
	//UPROPERTY(EditDefaultsOnly, Category = Projectile)
	//TArray<TSubclassOf<AAimRocketProjectile>> Projectiles;

	//Get's target info
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void LockTarget();

	//Get's Projectile type for WBP
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	int32 GetWeaponIndex() { return ProjectileIndex; };

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector MuzzleOffset;

	/** Sets default values for this component's properties */
	UTP_WeaponComponent();

	/** Attaches the actor to a FirstPersonCharacter */
	UFUNCTION(BlueprintCallable, Category="Weapon")
	void AttachWeapon(AAimRocketCharacter* TargetCharacter);

	/** Make the weapon Fire a Projectile */
	UFUNCTION(BlueprintCallable, Category="Weapon")
	void Fire();

	/** Sound to play each time we lock target */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	USoundBase* LockedSound = nullptr;

	//Aimed Block
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	ATestActor* LockedTarget = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileComponent")
	TSubclassOf<UProjectileComponent> ProjectileComponent;

	UPROPERTY(BlueprintReadOnly, Category = "ProjectileComponent")
	int32 ProjectileIndex = 0;


protected:
	/** Ends gameplay for this component. */
	UFUNCTION()
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
		

private:
	/** The Character holding this weapon*/
	AAimRocketCharacter* Character;
};
