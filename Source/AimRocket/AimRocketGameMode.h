// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AimRocketGameMode.generated.h"

UCLASS(minimalapi)
class AAimRocketGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAimRocketGameMode();
};



