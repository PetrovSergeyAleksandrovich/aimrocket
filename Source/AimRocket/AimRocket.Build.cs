// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class AimRocket : ModuleRules
{
	public AimRocket(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "DeveloperSettings", "RenderCore", "Niagara" });
	}
}
