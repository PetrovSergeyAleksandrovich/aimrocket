// Copyright Epic Games, Inc. All Rights Reserved.


#include "TP_WeaponComponent.h"
#include "GameFramework/PlayerController.h"
#include "AimRocketCharacter.h"
#include "Camera/PlayerCameraManager.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SceneComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraSystem.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponent, All, All)

// Sets default values for this component's properties
UTP_WeaponComponent::UTP_WeaponComponent()
{
	// Default offset from the character location for projectiles to spawn
	MuzzleOffset = FVector(100.0f, 0.0f, 10.0f);
}

void UTP_WeaponComponent::Fire()
{
	if(Character == nullptr || Character->GetController() == nullptr)
	{
		return;
	}

	// Try and fire a projectile
	if (ProjectileComponent)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			APlayerController* PlayerController = Cast<APlayerController>(Character->GetController());
			const FRotator SpawnRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = GetOwner()->GetActorLocation() + SpawnRotation.RotateVector(MuzzleOffset);
	
			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	
			// Spawn the projectile at the muzzle
			const auto ProjectileComp = Cast<UProjectileComponent>(ProjectileComponent.GetDefaultObject());
			if (ProjectileComp)
			{
				const auto Projectile = World->SpawnActor<AAimRocketProjectile>(ProjectileComp->Projectiles[ProjectileIndex], SpawnLocation, SpawnRotation, ActorSpawnParams);
				
				if (Projectile)
				{
					UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), Projectile->MuzzleFX, SpawnLocation, SpawnRotation, FVector(1.0f, 1.0f, 1.0f), true, true, ENCPoolMethod::None, false);
					
					if (LockedTarget)
					{
						Projectile->TargetPoint = LockedTarget->GetActorLocation();
						Projectile->ProjectileMovement->HomingTargetComponent = LockedTarget->GetMeshHomingTarget();
					}
				}
			}
			
		}
	}
	
	// Try and play the sound if specified
	if (ProjectileComponent)
	{
		const auto ProjectileComp = Cast<UProjectileComponent>(ProjectileComponent.GetDefaultObject());
		if (ProjectileComp)
		{
			const auto Projectile = Cast<AAimRocketProjectile>(ProjectileComp->Projectiles[ProjectileIndex].GetDefaultObject());
			if (Projectile)
			{
				const auto Sound = Cast<USoundBase>(Projectile->FireSound);
				if (Sound)
				{
					UGameplayStatics::PlaySoundAtLocation(this, Sound, Character->GetActorLocation());
				}
			}
		}
	}
	
	// Try and play a firing animation if specified
	if (ProjectileComponent)
	{
		const auto ProjectileComp = Cast<UProjectileComponent>(ProjectileComponent.GetDefaultObject());
		if (ProjectileComp)
		{
			const auto Projectile = Cast<AAimRocketProjectile>(ProjectileComp->Projectiles[ProjectileIndex].GetDefaultObject());
			if (Projectile)
			{
				const auto FireAnim = Cast<UAnimMontage>(Projectile->FireAnimation);
				if (FireAnim)
				{
					// Get the animation object for the arms mesh
					UAnimInstance* AnimInstance = Character->GetMesh1P()->GetAnimInstance();
					if (AnimInstance)
					{
						AnimInstance->Montage_Play(FireAnim, 1.f);
					}
				}
			}
		}
	}
}

void UTP_WeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(Character != nullptr)
	{
		// Unregister from the OnUseItem Event
		Character->OnUseItem.RemoveDynamic(this, &UTP_WeaponComponent::Fire);
	}
}

void UTP_WeaponComponent::AttachWeapon(AAimRocketCharacter* TargetCharacter)
{
	Character = TargetCharacter;
	if(Character != nullptr)
	{
		// Attach the weapon to the First Person Character
		FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
		GetOwner()->AttachToComponent(Character->GetMesh1P(),AttachmentRules, FName(TEXT("GripPoint")));

		// Register so that Fire is called every time the character tries to use the item being held
		Character->OnUseItem.AddDynamic(this, &UTP_WeaponComponent::Fire);
		Character->OnAimItem.AddDynamic(this, &UTP_WeaponComponent::LockTarget);
		Character->CurrentWeapon = Cast<UTP_WeaponComponent>(this);
	}
}


void UTP_WeaponComponent::LockTarget()
{
	UE_LOG(LogWeaponComponent, Display, TEXT("Try to lock target"));

	//checks if projectile can be set to homing target
	const auto ProjectileComp = Cast<UProjectileComponent>(ProjectileComponent.GetDefaultObject());
	if (ProjectileComp)
	{
		const auto Projectile = Cast<AAimRocketProjectile>(ProjectileComp->Projectiles[ProjectileIndex].GetDefaultObject());
		if (Projectile && !Projectile->ProjectileMovement->bIsHomingProjectile)
		{
			return;
		}
	}

	FVector TraceStart, TraceEnd;
	FRotator ViewRotation;

	if (Character->IsPlayerControlled())
	{
		const auto Controller = Character->GetController<APlayerController>();
		Controller->GetPlayerViewPoint(TraceStart, ViewRotation);
	}

	const FVector ShootDirection = ViewRotation.Vector();
	TraceEnd = TraceStart + ShootDirection * 15000.0f;

	FHitResult HitResult;
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(GetOwner());
	CollisionParams.bReturnPhysicalMaterial = true;
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);

	if (HitResult.bBlockingHit)
	{
		const auto NewTarget = Cast<ATestActor>(HitResult.GetActor());
		if (!NewTarget || NewTarget == LockedTarget) return;

		NewTarget->ChangeColorOnLocked();

		if (LockedTarget)
		{
			LockedTarget->ChangeColorOnUnlocked();
		}
		LockedTarget = Cast<ATestActor>(NewTarget);

		if (LockedSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, LockedSound, Character->GetActorLocation());
		}
	}
}





