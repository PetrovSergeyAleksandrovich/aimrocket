// Fill out your copyright notice in the Description page of Project Settings.


#include "TestActor.h"

// Sets default values
ATestActor::ATestActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	Mesh->SetupAttachment(GetRootComponent());
	Mesh->SetMaterial(0, MaterialDefault);

	MeshHomingTarget = CreateDefaultSubobject<UStaticMeshComponent>("HomingTargetMeshComponent");
	MeshHomingTarget->SetupAttachment(Mesh);;
}

// Called when the game starts or when spawned
void ATestActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATestActor::ChangeColorOnLocked()
{
	Mesh->SetMaterial(0, MaterialWhenTargeted);
}

void ATestActor::ChangeColorOnUnlocked()
{
	Mesh->SetMaterial(0, MaterialDefault);
}

